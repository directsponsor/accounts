Hello {{$name}},
Thank you for joining Direct Sponsor and participating in the project : {{$project->name}}. <br />
1- To sponsor this project, you will need to pay an amount of <strong>{{$project->amount}} in {{$project->currency}} </strong> which is equals to <strong>{{$project->euro_amount}} EUR.</strong>
Please make a payment with this amount to this Skrill account : {{$recipientSkrill}} <br />
2- Then follow this link to confirm your email address : <a href="{{URL::route('sponsors.confirm',$hash)}}">{{URL::route('sponsors.confirm',$hash)}}</a><br />
