<!doctype html>
<html>
    <head>
        <title> Control Panel </title>
        <meta charset='utf-8'>
        <!-- Style -->
        {{HTML::style('css/admin.css')}}
        <!-- jQuery & jQuery UI -->
        {{HTML::script('js/jquery.js')}}
        <!-- CKEditor -->
        {{HTML::script('js/ckeditor/ckeditor.js')}}
        <!-- DataTables jQuery Plugin -->
        {{HTML::script('js/jquery.dataTables.min.js')}}
        <script type="text/javascript">
            $(document).ready(function(){
                $('table.data').dataTable({
                    "aaSorting": [[ 0, "desc" ]]
                });
            });
        </script>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <a class='logo' href="#">{{ HTML::image('images/logo.png','Direct Sponsor'); }}</a>
                @include('layouts.elements.nav')
            </div>
            <div id="content">
                @if(isset($title)) <h1>{{ $title }} </h1> @endif
                @if(isset($error))
                    <p class="error">{{ $error }}</p>
                @endif
                @if(isset($success))
                    <p class="success">{{ $success }}</p>
                @endif
                @if(isset($info))
                    <p class="info">{{ $info }}</p>
                @endif
                @if(isset($notification))
                    <p class="error">{{ $notification }}</p>
                @endif
                {{ $content }}
            </div>
            <div id="footer">
            </div>
        </div>
    </body>
</html>
