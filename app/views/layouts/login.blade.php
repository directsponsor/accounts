<!doctype html>
<html>
    <head>
        <title> Login to Control Panel </title>
        <meta charset='utf-8'>
        {{HTML::style('css/admin.css')}}
        {{HTML::script('js/jquery.js')}}
    </head>
    <body>
        <div id="login">
            <h1>Login to Control Panel</h1>
            @if(isset($error))
                <p class="error">{{ $error }}</p>
            @endif
            @if(isset($success))
                <p class="success">{{ $success }}</p>
            @endif
            @if(isset($info))
                <p class="info">{{ $info }}</p>
            @endif
            {{ $content }}
            <a href="{{ URL::route('users.resendConfEmailForm') }}" class="add_item">Resend Confirmation Email</a>
            <a href="{{ URL::route('users.forgetPasswordForm') }}" class="add_item">Forget password?</a>
        </div>
    </body>
</html>