<?php $accountType = Auth::user()->account_type; ?>
@if($accountType == 'Admin')
<a href="{{ URL::route('projects.create') }}" class="add_item">New Project</a>
@endif
@if(!count($projects)) <table> @else <table class="data"> @endif 
    <thead><tr>
        <th>Name</th>
        @if($accountType == 'Sponsor')
        <th>Recipient</th>
        <th>Recipient Skrill Id</th>
        @else
        <th>Coordinator</th>
        @endif
        <th>Recipients</th>
        <th>Sponsors</th>
        <th></th>
    </tr></thead>
    <tbody>
    @if(!count($projects))
    <tr>
        <td colspan="5"><p>There is no projects yet ! </p></td>
    </tr>
    @endif
    @foreach($projects as $project)
    <tr>
        <td>{{$project->name}}</td>
        @if($accountType == 'Sponsor')
        <td>{{$project->recipientOfSponsor(Auth::user()->account->id)->user->username}}</td>
        <td>{{$project->recipientOfSponsor(Auth::user()->account->id)->skrill_acc}}</td>
        @else
        <td>{{$project->coordinator->user->username}}</td>
        @endif
        <td>{{$project->recipients->count()}}</td>
        <td>{{$project->sponsors->count()}}</td>
        <td>
            <a href="{{ URL::route('projects.show',$project->url) }}">Details</a>
            <a href="{{ URL::route('projects.spends.index',$project->id) }}"> Activities </a>
        @if($accountType == 'Sponsor')
            {{ Form::open(array('class'=>'inline', 'method'=>'delete','route'=>array('projects.withdraw',$project->id))),
                Form::submit('Withdraw'),
            Form::close() }}
        @endif
        @if($accountType == 'Admin')
            <a href="{{ URL::route('projects.invitations.index',$project->id) }}">Invitations</a>
            <a href="{{ URL::route('projects.edit',$project->url) }}">Edit</a>
            @if($project->open)
            {{ Form::open(array('class'=>'inline', 'method'=>'delete','route'=>array('projects.close',$project->id))),
                Form::submit('Close'),
            Form::close() }}
            @else
            {{ Form::open(array('class'=>'inline', 'method'=>'delete','route'=>array('projects.open',$project->id))),
                Form::submit('Reopen'),
            Form::close() }}
            @endif
        @endif
        </td>
    </tr>
    @endforeach
</tbody></table>