@if(!count($projects)) <table> @else <table class="data"> @endif 
    <thead><tr>
        <th>Name</th>
        <th>Amount</th>
        <th>Currency</th>
        <th></th>
    </tr></thead>
    <tbody>
    @if(!count($projects))
    <tr>
        <td colspan="4"><p>There is no projects yet ! </p></td>
    </tr>
    @endif
    @foreach($projects as $project)
    <tr>
        <td>{{$project->name}}</td>
        <td>{{$project->amount}}</td>
        <td>{{$project->currency}}</td>
        <td>
            <a href="{{ URL::route('projects.show',$project->url) }}">Details</a>
            <a href="{{URL::route('joinProjectAction',$project->id)}}"> Join this project </a>
        </td>
    </tr>
    @endforeach
</tbody></table>