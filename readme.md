This is an accounts backend built using the Laravel framework version 4. 
It provides 4 different user types : sponsors, coordinators, recipients and admins.

The role of the program is to make it easy for sponsors to directly pay recipients in a recurring fashion and keep track of what the donations are being used for.

Installation Instructions:

1. Check that you have the minimum requirements for running Laravel 4

2. Clone the repo with the source code

3. Due to shared host restrictions, vendor files and auto load files are included. Composer is not necessary unless new packages are added.

4. Copy the app/config-sample folder to app/config and make the changes to the database.php , app.php and mail.php as required

5. Run 'php artisan migrate' to create the DB structure

6. Run 'php artisan db:seed' to create an admin user with details : admin / password


The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)